#include <iostream>
#include <winsock.h>
#include "lesConstantes.h"

int main()
{
    WSADATA WSAData;
    WSAStartup(MAKEWORD(1, 0), &WSAData);

    SOCKET sock;
    SOCKADDR_IN sin;
    char buffer[255];
    char buffersend[15] = "Mauchamp";
    sin.sin_addr.s_addr = inet_addr(ip);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock == INVALID_SOCKET)
    {
        std::cout << "Erreur initialisation socket ; " << WSAGetLastError();
        return -2;
    }

    connect(sock, (SOCKADDR*)&sin, sizeof(sin));

    send(sock,buffersend, 14, 0);
    recv(sock, buffer, sizeof(buffer), 0);
    std::cout << buffer;

    closesocket(sock);
    WSACleanup();
    return 0;
}
