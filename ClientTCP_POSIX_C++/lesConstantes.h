#pragma once

#define IP_LOCALE "127.0.0.1"
#define IP_MODBUS212 "192.168.200.105"
#define IP_MODBUSASCENSEUR
#define PORT_TEST_SOCKET 13000
#define PORT_MODBUS 502

#define SERVEUR_TEST 1
#define SERVEUR_SIMULATEUR 2
#define SERVEUR_MODBUS 3
#define SERVEUR_ASCENSEUR 4

#define TYPE_SERVEUR (SERVEUR_TEST)


#if TYPE_SERVEUR == SERVEUR_TEST
const char* ip = IP_LOCALE;
const unsigned int port = PORT_TEST_SOCKET;
#endif

#if TYPE_SERVEUR == SERVEUR_SIMULATEUR
const char* ip = IP_MODBUS212;
const unsigned int port = PORT_MODBUS;
#endif

#if TYPE_SERVEUR == SERVEUR_MODBUS
const char* ip = IP_MODBUS212;
const unsigned int port = PORT_MODBUS;
#endif

#if TYPE_SERVEUR == SERVEUR_ASCENSEUR
const char* ip = IP_MODBUSASCENSEUR;
const unsigned int port = PORT_MODBUS;
#endif